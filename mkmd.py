import sys
import markdown

text = sys.stdin.read()
html = markdown.markdown(text)

sys.stdout.write(
    u"<html>\n<body>\n{}\n</body>\n</html>\n"
    .format(html))

This is a utility to package Kiln for Mac OS. Download Kiln for Mac OS
at [Kiln release page](https://gitlab.com/tezos-kiln/kiln/-/releases)

Building Kiln requires

- [Nix package manager](https://nixos.org/nix/)
- `Rosetta` binary translator in case you're using a mac with `arm64` CPU
- [`brew` package manager](https://brew.sh/) (installed under Rosetta in case
  you're using a mac with `arm64` CPU)

To build Kiln at specified revision/tag (e.g. `0.17.1-rc1`):

```
# On a mac with x86_64 CPU
./mkpkg 0.17.1-rc1
# On a mac with ARM CPU
BREW_PATH=/usr/local/bin/brew ./mkpkg 0.17.1-rc1
```

Alternatively, you can invoke the packaging script from anywhere using `nix run`:
```
nix --extra-experimental-features nix-command --extra-experimental-features flakes run gitlab:tezos-kiln/kiln-macos-pkg#mkpkg -- 0.17.1-rc1
```

Successfull build should produce `kiln-0.17.1-rc1.pkg` in `./build`
directory.

For MacOS specific Kiln configuration see [Kiln
README](./package/root/usr/local/kiln-nix/README.md)

Kiln's user interface is available at http://localhost:8444

Note that installer modifies power settings to prevent computer from sleeping
when display is off.

View logs via Applications > Utilities > Console, Log Reports, or
in a terminal:

```
tail -F ~/Library/Logs/Kiln/*.log
```

To uninstall run

```
/usr/local/kiln-nix/uninstall
```

Note that uninstaller doesn't remove data and log files in `~/Library/Kiln` and
`~/Library/Logs/Kiln`.

# Configure

To change network to `carthagenet`:

```
echo carthagenet > ~/Library/Kiln/config/network
launchctl stop tezos.kiln
launchctl start tezos.kiln
```

then reload Kiln UI web page (it may reconnect without reload but it does not
properly refresh nodes information)

By default Kiln UI web page is accessible only from the same computer. Edit
`~/Library/Kiln/env` if you want to enable access from other computers and/or
change port:

```
KILN_WEB_UI_ADDR=0.0.0.0
KILN_WEB_UI_PORT=8444
```

# Caveats

- This installer is for MacOS Catalina only. It won't work for earlier
  MacOS versions.

- User must be logged in. Kiln needs access to ledger device to bake. Ledger
  device on MacOS registers as HID (Human Interface Device) and is only
  accessible to root or _current_ logged-in user. Kiln can't run as root. Kiln
  may be reconfigured to run as daemon without requiring user to be logged in,
  but only as a monitor, it won't be able to bake.

- Kiln installed via this package won't work on a machine with Nix already
  installed. Installation adds `/nix` as a symlink via `/etc/synthetic.conf`
  since `/` is not writable on MacOS Catalina, but only if symlink for `nix` not
  already defined.
  See https://github.com/NixOS/nix/issues/2925

# Run as Daemon
Kiln may be configured to run as daemon so that user is not required to be
logged in, but it won't have access to ledger device and thus won't be able to
bake. To switch to Kiln running as daemon run

```
/usr/local/kiln-nix/install-as-daemon
```

When running as daemon, user interface is at http://localhost:8445

{
  description = "A flake with dependencies needed to package Kiln for macOS";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils, ... }@inputs:
    flake-utils.lib.eachSystem [ "x86_64-darwin" "aarch64-darwin" ] (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        python3-with-packages = pkgs.python3.withPackages (p: with p; [
          markdown
        ]);
      in {
        devShells.default = pkgs.mkShell {
          packages = with pkgs; [
            python3-with-packages git wget rsync gnused which
          ];
        };
        apps.mkpkg = {
          type = "app";
          program = ./mkpkg;
        };
    });
}
